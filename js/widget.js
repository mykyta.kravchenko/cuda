(function () {
  const searchForm = document.createElement("div");
  searchForm.setAttribute("class", "search-form");
  searchForm.attachShadow({ mode: "open" });
  Object.assign(searchForm.style, {
    width: "400px",
    height: "180px",
    background: "rgba(0, 0, 0, .9)",
    position: "fixed",
    top: 0,
    right: 0,
    boxSizing: "border-box",
    padding: "20px",
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
    alignItems: "center",
    zIndex: 10000,
  });

  searchForm.shadowRoot.innerHTML = `
    <style>
      .search-form__header {
        margin: 0; 
        font-weight: normal; 
        font-size: 18px; 
        flex-basis: 75%; 
        color: #fff;
        user-select: none;
      }
    
      .search-form__header:hover {
        cursor: move;
      }
    
      .search-form__selector-input {
        box-sizing: border-box;
        padding-left: 10px; 
        flex-basis: 73%; 
        height: 30px; 
        outline: none; 
        background: none;
        border: 1px solid #fff;
        border-radius: 7px;
        color: #fff;
      }

      .search-form__close-btn {
        flex-basis: 5%;
        color: #fff;
        background: none;
        border: none;
      }
      
      .btn {
        height: 30px; 
        flex-basis: 20%; 
        outline: none;
        background: none;
        border: 1px solid #fff;
        border-radius: 7px;
        color: #fff;
      }
    
      .btn:active {
        background: #fff;
        color: #000;
      }
    
      .btn:disabled {
        opacity: .2;
      }
    
      button:hover {
        cursor: pointer;
      }
    </style>
    <h2 class="search-form__header">Search node element</h2>
    <button class="search-form__close-btn">X</button>
    <input class="search-form__selector-input" type="text">
    <button class="search-form__search-btn btn">Search</button>
    <button class="search-form__prev-btn btn" disabled="true">Prev</button>
    <button class="search-form__next-btn btn" disabled="true">Next</button>
    <button class="search-form__parent-btn btn" disabled="true">Parent</button>
    <button class="search-form__child-btn btn" disabled="true">Child</button>
  `;

  const formHeader = searchForm.shadowRoot.querySelector(".search-form__header");
  const formCloseBtn = searchForm.shadowRoot.querySelector(".search-form__close-btn");
  const formInput = searchForm.shadowRoot.querySelector(".search-form__selector-input");
  const formSearchBtn = searchForm.shadowRoot.querySelector(".search-form__search-btn");
  const formPrevBtn = searchForm.shadowRoot.querySelector(".search-form__prev-btn");
  const formNextBtn = searchForm.shadowRoot.querySelector(".search-form__next-btn");
  const formParentBtn = searchForm.shadowRoot.querySelector(".search-form__parent-btn");
  const formChildBtn = searchForm.shadowRoot.querySelector(".search-form__child-btn");

  document.body.insertAdjacentElement("beforeend", searchForm);

  const scrollOptions = {
    behavior: "smooth",
    block: "center",
    inline: "center",
  };
  let searchedElement;

  function changeTargetButtonsStatus(node) {
    formPrevBtn.disabled = node.previousElementSibling ? false : true;
    formNextBtn.disabled = node.nextElementSibling ? false : true;
    formParentBtn.disabled = node.parentElement ? false : true;
    formChildBtn.disabled = node.firstElementChild ? false : true;
  }

  function targetButtonsHandler(whereTo) {
    searchedElement.style.border = 'none';

    if (whereTo === "prev") {
      searchedElement = searchedElement.previousElementSibling;
    } else if (whereTo === "next") {
      searchedElement = searchedElement.nextElementSibling;
    } else if (whereTo === "parent") {
      searchedElement = searchedElement.parentElement;
    } else if (whereTo === "child") {
      searchedElement = searchedElement.firstElementChild;
    }

    searchedElement.style.border = '3px solid red';
    searchedElement.scrollIntoView(scrollOptions);
    changeTargetButtonsStatus(searchedElement);
  }

  formSearchBtn.addEventListener("click", () => {
    const selector = formInput.value;

    if (!selector) {
      alert("Enter selector");
      return;
    }

    if (searchedElement) {
      searchedElement.style.border = 'none';
    }

    searchedElement = document.querySelector(selector);
    if (!searchedElement) {
      alert("Element wasn't found");
      return;
    }

    searchedElement.style.border = '3px solid red';
    searchedElement.scrollIntoView(scrollOptions);
    changeTargetButtonsStatus(searchedElement);
  });

  formPrevBtn.addEventListener("click", () => targetButtonsHandler("prev"));
  formNextBtn.addEventListener("click", () => targetButtonsHandler("next"));
  formParentBtn.addEventListener("click", () => targetButtonsHandler("parent"));
  formChildBtn.addEventListener("click", () => targetButtonsHandler("child"));

  formCloseBtn.addEventListener("click", () => {
    if (searchedElement) {
      searchedElement.style.border = 'none';
    }

    formSearchBtn.removeEventListener("click", () => {});
    formPrevBtn.removeEventListener("click", () => targetButtonsHandler("prev"));
    formNextBtn.removeEventListener("click", () => targetButtonsHandler("next"));
    formParentBtn.removeEventListener("click", () => targetButtonsHandler("parent"));
    formChildBtn.removeEventListener("click", () => targetButtonsHandler("child"));
    formCloseBtn.removeEventListener("click", () => {});
    formHeader.removeEventListener('mousedown', () => {});

    searchForm.remove();
  });

  formHeader.addEventListener("mousedown", (event) => {
    formHeader.ondragstart = function() {
      return false;
    };

    const shiftX = event.clientX - searchForm.getBoundingClientRect().left;
    const shiftY = event.clientY - searchForm.getBoundingClientRect().top;

    moveFormAt(event.clientX, event.clientY);
    function moveFormAt(clientX, clientY) {
      searchForm.style.left = clientX - shiftX + "px";
      searchForm.style.top = clientY - shiftY + "px";
    }

    function onMouseMove(event) {
      moveFormAt(event.clientX, event.clientY);
    }

    function stopFormMove() {
      document.removeEventListener("mousemove", onMouseMove);
      document.removeEventListener("mouseup", stopFormMove);
    }

    document.addEventListener("mousemove", onMouseMove);
    document.addEventListener("mouseup", stopFormMove);
  });
})();

/* 
javascript:void%20function(){(function(){function%20a(a){h.disabled=!a.previousElementSibling,i.disabled=!a.nextElementSibling,j.disabled=!a.parentElement,k.disabled=!a.firstElementChild}function%20b(b){m.style.border=%22none%22,%22prev%22===b%3Fm=m.previousElementSibling:%22next%22===b%3Fm=m.nextElementSibling:%22parent%22===b%3Fm=m.parentElement:%22child%22==b%26%26(m=m.firstElementChild),m.style.border=%223px%20solid%20red%22,m.scrollIntoView(l),a(m)}const%20c=document.createElement(%22div%22);c.setAttribute(%22class%22,%22search-form%22),c.attachShadow({mode:%22open%22}),Object.assign(c.style,{width:%22400px%22,height:%22180px%22,background:%22rgba(0,%200,%200,%20.9)%22,position:%22fixed%22,top:0,right:0,boxSizing:%22border-box%22,padding:%2220px%22,display:%22flex%22,flexWrap:%22wrap%22,justifyContent:%22space-between%22,alignItems:%22center%22,zIndex:1e4}),c.shadowRoot.innerHTML=`
%20%20%20%20%3Cstyle%3E
%20%20%20%20%20%20.search-form__header%20{
%20%20%20%20%20%20%20%20margin:%200;%20
%20%20%20%20%20%20%20%20font-weight:%20normal;%20
%20%20%20%20%20%20%20%20font-size:%2018px;%20
%20%20%20%20%20%20%20%20flex-basis:%2075%25;%20
%20%20%20%20%20%20%20%20color:%20%23fff;
%20%20%20%20%20%20%20%20user-select:%20none;
%20%20%20%20%20%20}
%20%20%20%20
%20%20%20%20%20%20.search-form__header:hover%20{
%20%20%20%20%20%20%20%20cursor:%20move;
%20%20%20%20%20%20}
%20%20%20%20
%20%20%20%20%20%20.search-form__selector-input%20{
%20%20%20%20%20%20%20%20box-sizing:%20border-box;
%20%20%20%20%20%20%20%20padding-left:%2010px;%20
%20%20%20%20%20%20%20%20flex-basis:%2073%25;%20
%20%20%20%20%20%20%20%20height:%2030px;%20
%20%20%20%20%20%20%20%20outline:%20none;%20
%20%20%20%20%20%20%20%20background:%20none;
%20%20%20%20%20%20%20%20border:%201px%20solid%20%23fff;
%20%20%20%20%20%20%20%20border-radius:%207px;
%20%20%20%20%20%20%20%20color:%20%23fff;
%20%20%20%20%20%20}

%20%20%20%20%20%20.search-form__close-btn%20{
%20%20%20%20%20%20%20%20flex-basis:%205%25;
%20%20%20%20%20%20%20%20color:%20%23fff;
%20%20%20%20%20%20%20%20background:%20none;
%20%20%20%20%20%20%20%20border:%20none;
%20%20%20%20%20%20}
%20%20%20%20%20%20
%20%20%20%20%20%20.btn%20{
%20%20%20%20%20%20%20%20height:%2030px;%20
%20%20%20%20%20%20%20%20flex-basis:%2020%25;%20
%20%20%20%20%20%20%20%20outline:%20none;
%20%20%20%20%20%20%20%20background:%20none;
%20%20%20%20%20%20%20%20border:%201px%20solid%20%23fff;
%20%20%20%20%20%20%20%20border-radius:%207px;
%20%20%20%20%20%20%20%20color:%20%23fff;
%20%20%20%20%20%20}
%20%20%20%20
%20%20%20%20%20%20.btn:active%20{
%20%20%20%20%20%20%20%20background:%20%23fff;
%20%20%20%20%20%20%20%20color:%20%23000;
%20%20%20%20%20%20}
%20%20%20%20
%20%20%20%20%20%20.btn:disabled%20{
%20%20%20%20%20%20%20%20opacity:%20.2;
%20%20%20%20%20%20}
%20%20%20%20
%20%20%20%20%20%20button:hover%20{
%20%20%20%20%20%20%20%20cursor:%20pointer;
%20%20%20%20%20%20}
%20%20%20%20%3C/style%3E
%20%20%20%20%3Ch2%20class=%22search-form__header%22%3ESearch%20node%20element%3C/h2%3E
%20%20%20%20%3Cbutton%20class=%22search-form__close-btn%22%3EX%3C/button%3E
%20%20%20%20%3Cinput%20class=%22search-form__selector-input%22%20type=%22text%22%3E
%20%20%20%20%3Cbutton%20class=%22search-form__search-btn%20btn%22%3ESearch%3C/button%3E
%20%20%20%20%3Cbutton%20class=%22search-form__prev-btn%20btn%22%20disabled=%22true%22%3EPrev%3C/button%3E
%20%20%20%20%3Cbutton%20class=%22search-form__next-btn%20btn%22%20disabled=%22true%22%3ENext%3C/button%3E
%20%20%20%20%3Cbutton%20class=%22search-form__parent-btn%20btn%22%20disabled=%22true%22%3EParent%3C/button%3E
%20%20%20%20%3Cbutton%20class=%22search-form__child-btn%20btn%22%20disabled=%22true%22%3EChild%3C/button%3E
%20%20`;const%20d=c.shadowRoot.querySelector(%22.search-form__header%22),e=c.shadowRoot.querySelector(%22.search-form__close-btn%22),f=c.shadowRoot.querySelector(%22.search-form__selector-input%22),g=c.shadowRoot.querySelector(%22.search-form__search-btn%22),h=c.shadowRoot.querySelector(%22.search-form__prev-btn%22),i=c.shadowRoot.querySelector(%22.search-form__next-btn%22),j=c.shadowRoot.querySelector(%22.search-form__parent-btn%22),k=c.shadowRoot.querySelector(%22.search-form__child-btn%22);document.body.insertAdjacentElement(%22beforeend%22,c);const%20l={behavior:%22smooth%22,block:%22center%22,inline:%22center%22};let%20m;g.addEventListener(%22click%22,()=%3E{const%20b=f.value;return%20b%3F(m%26%26(m.style.border=%22none%22),m=document.querySelector(b),m%3Fvoid(m.style.border=%223px%20solid%20red%22,m.scrollIntoView(l),a(m)):void%20alert(%22Element%20wasn't%20found%22)):void%20alert(%22Enter%20selector%22)}),h.addEventListener(%22click%22,()=%3Eb(%22prev%22)),i.addEventListener(%22click%22,()=%3Eb(%22next%22)),j.addEventListener(%22click%22,()=%3Eb(%22parent%22)),k.addEventListener(%22click%22,()=%3Eb(%22child%22)),e.addEventListener(%22click%22,()=%3E{m%26%26(m.style.border=%22none%22),g.removeEventListener(%22click%22,()=%3E{}),h.removeEventListener(%22click%22,()=%3Eb(%22prev%22)),i.removeEventListener(%22click%22,()=%3Eb(%22next%22)),j.removeEventListener(%22click%22,()=%3Eb(%22parent%22)),k.removeEventListener(%22click%22,()=%3Eb(%22child%22)),e.removeEventListener(%22click%22,()=%3E{}),d.removeEventListener(%22mousedown%22,()=%3E{}),c.remove()}),d.addEventListener(%22mousedown%22,a=%3E{function%20b(a,b){c.style.left=a-g+%22px%22,c.style.top=b-h+%22px%22}function%20e(a){b(a.clientX,a.clientY)}function%20f(){document.removeEventListener(%22mousemove%22,e),document.removeEventListener(%22mouseup%22,f)}d.ondragstart=function(){return!1};const%20g=a.clientX-c.getBoundingClientRect().left,h=a.clientY-c.getBoundingClientRect().top;b(a.clientX,a.clientY),document.addEventListener(%22mousemove%22,e),document.addEventListener(%22mouseup%22,f)})})()}();
*/