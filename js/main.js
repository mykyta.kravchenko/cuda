const burgerBtn = document.querySelector(".welcome-header_burger-btn");
const menu = document.querySelector(".welcome-header_menu");

burgerBtn.addEventListener("click", () => {
  menu.classList.toggle("wecome-header_menu--opened");
  burgerBtn.classList.toggle("welcome-header_burger-btn_active");
});
